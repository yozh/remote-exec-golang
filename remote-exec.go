package main

import (
	"bytes"
	"encoding/json"
	"github.com/eclipse/paho.mqtt.golang"
	"gitlab.com/yozh/google-iot-mqtt-client"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

const ProjectId = "massive-physics-671"
const CloudRegion = "europe-west1"
const RegistryId = "yozh-device-registry"
const DeviceId = "remote-exec"
const DevicePath = "projects/" + ProjectId + "/locations/" + CloudRegion + "/registries/" + RegistryId + "/devices/" + DeviceId
const CommandsTopic = "/devices/" + DeviceId + "/commands/#"
const StateTopic = "/devices/" + DeviceId + "/state"

func main() {
	mqttClient := google_iot_mqtt_client.GoogleMqttClient{}
	e := mqttClient.Connect(ProjectId, DevicePath, "./resources/rsa_private.pem", "./resources/google-certs/roots.pem")
	if e != nil {
		log.Fatal("Cannot connect to MQTT", e)
	}

	mqttClient.Subscribe(CommandsTopic, 1, subscribeHandler)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	for {
		s := <-c
		log.Println(s)
		if s == syscall.SIGTERM || s == syscall.SIGINT {
			break
		}
	}
}

func subscribeHandler(client mqtt.Client, message mqtt.Message) {
	defer message.Ack()

	fullCommand := make(map[string]string)
	e := json.Unmarshal(message.Payload(), &fullCommand)
	if e != nil {
		log.Println("Error unmarshalling message", e)
	} else {
		log.Println("Received command", fullCommand)

		fields := strings.Fields(fullCommand["message"])
		cmd := exec.Command(fields[0], fields[1:]...)

		var out, err bytes.Buffer
		cmd.Stdout = &out
		cmd.Stderr = &err
		e := cmd.Start()

		if e != nil {
			errorText := string(e.Error())
			errorText += "\n" + string(err.Bytes())
			log.Println("Could not execute command:", errorText)
			log.Println(errorText)

			client.Publish(StateTopic, 1, false, makeResponseJson(errorText, fullCommand["chatId"]))
		} else {
			result := ""

			done := make(chan error, 1)
			go func() {
				done <- cmd.Wait()
			}()

			select {
			case <-time.After(5 * time.Second):
				if err := cmd.Process.Kill(); err != nil {
					log.Println("failed to kill process: ", err)
				} else {
					result += "Process killed by timeout\n"
				}
			case err := <-done:
				if err != nil {
					log.Println("process finished with error = %v", err)
					result += string(err.Error()) + "\n"
				}
			}
			result += string(err.Bytes()) + "\n"
			result += string(out.Bytes())

			log.Println(result)

			client.Publish(StateTopic, 1, false, makeResponseJson(result, fullCommand["chatId"]))
		}
	}
}

func makeResponseJson(message string, chatId string) []byte {
	result, _ := json.Marshal(map[string]string{
		"text":    message,
		"chat_id": chatId,
	})
	return result
}
