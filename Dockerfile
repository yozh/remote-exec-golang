FROM alpine:3.6
WORKDIR /app/
EXPOSE 8883
RUN adduser -D remoter
COPY remote-exec /app/
COPY resources /app/resources/
RUN chmod -R a=rx .
USER remoter
CMD ["./remote-exec"]