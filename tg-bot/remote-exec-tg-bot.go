package main

import (
	"bytes"
	"cloud.google.com/go/pubsub"
	"context"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"golang.org/x/oauth2/google"
	cloudiot "google.golang.org/api/cloudiot/v1"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
)

const ProjectId = "massive-physics-671"
const CloudRegion = "europe-west1"
const RegistryId = "yozh-device-registry"
const DeviceId = "remote-exec"
const DevicePath = "projects/" + ProjectId + "/locations/" + CloudRegion + "/registries/" + RegistryId + "/devices/" + DeviceId

const Subscription = "remote-exec-results"
const TgBotApiKey = "654544542:AAGLpWZC9BeZZY6dP58XA7R-srk85pn-RMA"

var TgApiUrl = "https://api.telegram.org/bot" + TgBotApiKey + "/%v"

func main() {
	var wg sync.WaitGroup

	wg.Add(2)
	go listenToStateSubscription(wg)
	go launchBotServer(wg)

	wg.Wait()
}

func launchBotServer(wg sync.WaitGroup) {
	defer wg.Done()

	var lastUpdateId float64 = 0
	for {
		body := map[string]interface{}{"limit": "1", "timeout": "60"}
		if lastUpdateId != 0 {
			body["offset"] = floatToStr(lastUpdateId)
		}
		response, _ := makeTgRequest("getUpdates", body)

		if response != nil {
			lastUpdateId, _ = response["update_id"].(float64)
			lastUpdateId++

			if response["message"] != nil && response["message"].(map[string]interface{})["text"] != nil {
				messageText := response["message"].(map[string]interface{})["text"].(string)
				handleMessage(response["message"].(map[string]interface{})["chat"].(map[string]interface{})["id"].(float64), messageText)
			} else if response["edited_message"] != nil && response["edited_message"].(map[string]interface{})["text"] != nil {
				messageText := response["edited_message"].(map[string]interface{})["text"].(string)
				handleMessage(response["edited_message"].(map[string]interface{})["chat"].(map[string]interface{})["id"].(float64), messageText)
			}
		}
	}
	//body := make(map[string]string)
	//body["chat_id"] = "284818739"
	//body["text"] = "Hello from Telegram bot in golang"
	//makeTgRequest("sendMessage", body)
}

func handleMessage(chatId float64, messageText string) {
	log.Println("Message:", messageText)
	if strings.Index(messageText, "/start") == 0 {
		message := map[string]interface{}{
			"chat_id": floatToStr(chatId),
			"text": "Привет, я бот на голанге, который шлёт linux команды го-программе на распберри через google iot по mqtt. Типа урезанный ssh. " +
				"Введите, например, pwd, ls -l или ifconfig",
			"reply_markup": map[string]bool{"force_reply": true},
		}
		_, _ = makeTgRequest("sendMessage", message)
	} else {
		deviceResponse, e := sendCommandToDevice(makeCommandJson(messageText, floatToStr(chatId)))
		if e != nil {
			log.Println("Cannot send command: ", e)
		} else {
			log.Println(deviceResponse)
		}
	}
}

func makeCommandJson(message string, chatId string) string {
	result, _ := json.Marshal(map[string]string{
		"message": message,
		"chatId":  chatId,
	})
	return string(result)
}

func floatToStr(number float64) string {
	return strconv.FormatFloat(number, 'f', -1, 64)
}

func makeTgRequest(methodName string, body map[string]interface{}) (map[string]interface{}, error) {
	jsonBody, e := json.Marshal(body)
	if e == nil {
		response, e := http.Post(fmt.Sprintf(TgApiUrl, methodName), "application/json", bytes.NewBuffer(jsonBody))
		if e != nil {
			log.Println("Error sending request", e)
			return nil, e
		} else {
			log.Println("Response: ", response)

			var data []byte
			defer response.Body.Close()
			data, e := ioutil.ReadAll(response.Body)

			if e != nil {
				log.Println("Error reading response body", e)
				return nil, e
			} else {
				log.Println("Got response body ", string(data))
				jsonResponse := make(map[string]interface{})
				e := json.Unmarshal(data, &jsonResponse)
				if e != nil {
					log.Println("Error unmarshalling response", e)
				}

				var update map[string]interface{} = nil

				switch v := jsonResponse["result"].(type) {
				case map[string]interface{}:
					{
						update = v
					}
				case []interface{}:
					{
						updates := jsonResponse["result"].([]interface{})

						if len(updates) > 0 {
							update = updates[0].(map[string]interface{})
						}
					}
				}

				return update, e
			}
		}
	} else {
		log.Println("Error marshalling body into JSON", e)
		return nil, e
	}
}

func listenToStateSubscription(wg sync.WaitGroup) {
	defer wg.Done()
	ctx := context.Background()
	client, err := pubsub.NewClient(ctx, ProjectId)
	if err != nil {
		log.Fatalf("Could not create pubsub Client: %v", err)
	}
	sub := client.Subscription(Subscription)
	cctx, _ := context.WithCancel(ctx)
	err = sub.Receive(cctx, handleStateMessage())
	if err != nil {
		log.Fatal("Cannot receive messages: ", err)
	}
}

func handleStateMessage() func(ctx context.Context, msg *pubsub.Message) {
	return func(ctx context.Context, msg *pubsub.Message) {
		msg.Ack()
		messageText := string(msg.Data)
		log.Printf("Got message: %q\n", messageText)

		var message = make(map[string]interface{})
		e := json.Unmarshal(msg.Data, &message)

		if e != nil {
			log.Println("Cannot unmarshal response", e)
		} else {
			if len(strings.TrimSpace(message["text"].(string))) == 0 {
				message["text"] = "[no output]"
			}

			message["reply_markup"] = map[string]bool{
				"force_reply": true,
			}
			_, e := makeTgRequest("sendMessage", message)
			if e != nil {
				log.Println("Cannot send request", e)
			}
		}
	}
}

func sendCommandToDevice(sendData string) (*cloudiot.SendCommandToDeviceResponse, error) {
	ctx := context.Background()
	httpClient, err := google.DefaultClient(ctx, cloudiot.CloudPlatformScope)
	if err != nil {
		return nil, err
	}
	client, err := cloudiot.New(httpClient)
	if err != nil {
		return nil, err
	}

	req := cloudiot.SendCommandToDeviceRequest{
		BinaryData: b64.StdEncoding.EncodeToString([]byte(sendData)),
	}

	response, err := client.Projects.Locations.Registries.Devices.SendCommandToDevice(DevicePath, &req).Do()
	if err != nil {
		return nil, err
	}

	log.Println("Sent command to device")

	return response, nil
}
